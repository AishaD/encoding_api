var base64 = require('base-64');
var utf8 = require('utf8');
const bodyParser = require('body-parser');
const express = require("express");
const app = express();
app.use(bodyParser.json());
app.use(express.json());


exports.encoding = (req, bodyParser, res) => {

    var text = req.body.data;
    console.log(req.body.data);

    var bytes = utf8.encode(text);
    var text = base64.encode(bytes);

   if(!text){
        console.log("Unable to encode");
        
    }
    else {
        console.log(`Encoded text is ${text}`)
    }

     
};

exports.decoding = (req, res) => {

    var text = req.body.data;
    var bytes = base64.decode(text);
    var text = utf8.decode(bytes);

    if(!text){
        console.log("Unable to decode");
        
    }
    else {
        console.log(`Decoded text is ${text}`)
    }
};