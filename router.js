var express = require("express");
var router = express.Router();

const {encoding, decoding} = require("../controller/controller")

router.post("/encode/str", encoding);
router.post("/decode/str", decoding);

module.exports = router;
