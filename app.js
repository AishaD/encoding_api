const encodeRoute = require("./router/router");
const bodyParser = require("body-parser");
const express = require("express");
const app = express();


app.use(express.json());
app.use(bodyParser.json());

app.use("/api", encodeRoute);
app.use(bodyParser.urlencoded({ extended: true }));


const port = process.env.PORT || 8000;

app.listen(port, () => {
    console.log(`app is running at ${port}`);
});
